import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = new String(userInput.nextLine());
        System.out.println("Last Name:");
        String lastName = new String(userInput.nextLine());
        System.out.println("First Subject Grade:");
        double firstSubjGrade = new Double(userInput.nextLine());
        System.out.println("Second Subject Grade:");
        double secondSubjGrade = new Double(userInput.nextLine());
        System.out.println("Third Subject Grade:");
        double thirdSubjGrade = new Double(userInput.nextLine());

        double average = ((firstSubjGrade + secondSubjGrade + thirdSubjGrade)/3);

        System.out.println("Good day, " + firstName + ".");
        System.out.println("Your grade average is " + average);
    }
}
